﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Runtime.InteropServices;

namespace QuakeConsole
{
    internal static class Native
    {
        const uint CF_UNICODETEXT = 13;

        [DllImport("user32.dll")]
        private static extern ushort GetKeyState(int keyCode);
        [DllImport("user32.dll")]
        private static extern IntPtr GetClipboardData(uint uFormat);
        [DllImport("user32.dll")]
        private static extern IntPtr SetClipboardData(uint uFormat, IntPtr data);
        [DllImport("user32.dll")]
        private static extern bool IsClipboardFormatAvailable(uint format);
        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool OpenClipboard(IntPtr hWndNewOwner);
        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool CloseClipboard();
        [DllImport("kernel32.dll")]
        private static extern IntPtr GlobalAlloc(uint uFlags, UIntPtr dwBytes);
        [DllImport("kernel32.dll")]
        private static extern IntPtr GlobalFree(IntPtr hMem);
        [DllImport("kernel32.dll")]
        private static extern IntPtr GlobalLock(IntPtr hMem);
        [DllImport("kernel32.dll")]
        private static extern bool GlobalUnlock(IntPtr hMem);
        [DllImport("kernel32.dll", EntryPoint = "CopyMemory", SetLastError = false)]
        private static extern void CopyMemory(IntPtr dest, IntPtr src, uint count);

        // Ref: http://users.cis.fiu.edu/~downeyt/cop4226/toggled.html
        public static bool IsKeyToggled(Keys key)
        {
            try
            {
                var nKey = (int) key;
                return (GetKeyState(nKey) & 0x01) == 1;
            }
            catch
            {
                return false;
            }
        }

        public static string GetClipboardText()
        {
            try
            {
                return GetClipboardTextImpl() ?? "";
            }
            catch
            {
                return "";
            }
        }

        public static void SetClipboardText(string value)
        {
            try
            {
                SetClipboardTextImpl(value);
            }
            catch
            {
            }
        }

        // Ref: http://stackoverflow.com/a/5945476/1466456
        private static string GetClipboardTextImpl() {
            return TextCopy.ClipboardService.GetText();
        }

        // Ref: http://stackoverflow.com/a/24698804/1466456
        private static bool SetClipboardTextImpl(string message)
        {
            if (message == null)
                return false;

            TextCopy.ClipboardService.SetText(message);

            return true;
        }
    }
}
